<img src="easybell.gif">

#Before start:
```
1. Copy .env.example as a .env
2. Setup DB credentials
3. composer install
4. npm install && run dev
5. php artisan migrate --seed
6. Setup local web server or "php artisan serve"
```
