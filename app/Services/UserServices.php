<?php

namespace App\Services;

use App\Http\Requests\StoreUserRequest;
use App\Models\User;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;

class UserServices
{
    public static function index()
    {
        return UserRepository::all();
    }

    public static function store(StoreUserRequest $request)
    {
        return UserRepository::create($request->validated());
    }

    public static function update(StoreUserRequest $request, User $user)
    {
        return $user->update(
            $request->validated()
        );
    }

    public static function delete(User $user)
    {
        $user->delete();
    }
}
