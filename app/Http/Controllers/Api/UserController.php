<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreUserRequest;
use App\Models\User;
use App\Services\UserServices;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function index()
    {
        return UserServices::index();
    }

    public function store(StoreUserRequest $request)
    {
        return UserServices::store($request);
    }

    public function show(User $user)
    {
        //I would also like to add DTO if frontend need data in another format
        return $user;
    }

    public function update(StoreUserRequest $request, User $user)
    {
        return UserServices::update($request, $user);
    }

    public function destroy(User $user)
    {
        UserServices::delete($user);

        return response()->noContent();
    }
}
