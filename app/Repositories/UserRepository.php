<?php

namespace App\Repositories;

use App\Models\User;


class UserRepository extends BaseRepository
{
    protected User $model;

    static protected string $modelClass = User::class;

    public function __construct()
    {
        $this->model = new User;
    }
}
