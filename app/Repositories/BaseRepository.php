<?php

namespace App\Repositories;

use Illuminate\Database\Eloquent\Builder;

abstract class BaseRepository
{
    static public function __callStatic($method, $parameters)
    {
        return static::$modelClass::{$method}(...$parameters);
    }

    public function __call($method, $parameters)
    {
        return $this->model->{$method}(...$parameters);
    }
}
