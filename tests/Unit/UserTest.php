<?php

namespace Tests\Unit;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class UserTest extends TestCase
{
    public function test_seed_users()
    {
        $this->assertTrue((bool) $this->seed());
    }

    public function test_get_all_users_api_endpoint()
    {
        $response = $this->get('/api/users');

        $response->assertStatus(200);
    }

    public function test_create_user_api_endpoint()
    {
        $mockRequestData = [
            'first_name' => 'test_fname',
            'last_name' => 'test_lname',
        ];

        $response = $this->post('/api/users', $mockRequestData);

        $response->assertStatus(201);
        $this->assertDatabaseHas('users', $mockRequestData);
    }

    public function test_show_user_api_endpoint()
    {
        $user = User::inRandomOrder()->first();

        $response = $this->get('/api/users/' . $user->id);

        $response->assertStatus(200);
    }

    public function test_update_user_api_endpoint()
    {
        $user = User::inRandomOrder()->first();
        $mockRequestData = [
            'first_name' => 'test_fname_updated',
            'last_name' => 'test_lname_updated',
        ];

        $response = $this->patch('/api/users/' . $user->id, $mockRequestData);

        $response->assertStatus(200);
        $this->assertDatabaseHas('users', $mockRequestData);
    }

    public function test_delete_user_api_endpoint()
    {
        $user = User::inRandomOrder()->first();

        $response = $this->delete('/api/users/' . $user->id);

        $response->assertStatus(204);
        $this->assertDatabaseMissing('users', [
            'id' => $user->id,
        ]);
    }
}
