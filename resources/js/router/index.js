import { createRouter, createWebHistory } from 'vue-router'

import Overview from '../Components/Overview'
import User from '../Components/User'

const routes = [
    {
        path: "/",
        redirect: {
            name: 'overview'
        }
    },
    {
        name: "overview",
        path: "/overview",
        component: Overview,
    },
    {
        name: "user",
        path: "/user/:id?",
        component: User,
        props: true
    },
]

export default createRouter({
    history: createWebHistory(),
    routes
})
