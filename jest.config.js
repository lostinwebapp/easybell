module.exports = {
    testMatch: [
        '**/?(*.)+(spec|test).(js|ts|tsx)',
    ],
    preset: '@vue/cli-plugin-unit-jest/presets/no-babel'
}
